// Includes
#include <inttypes.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#include "raylib/src/raylib.h"
#include "raylib/src/raymath.h"

// Pre-Processing
#ifndef BOID_COUNT
#define BOID_COUNT 1000
#endif
#ifndef THREAD_COUNT
#define THREAD_COUNT 1
#endif

#define MAX(x, y) (x > y ? x : y)
#define MIN(x, y) (x < y ? x : y)

// Types
typedef struct {
  Vector2 pos, dir, new_dir;
} boid_t;

// Static Variables
static float separation_weight = 1, alignment_weight = 4, cohesion_weight = 200,
             target_weight = 2;
static float range, range_squared;
static boid_t boids[BOID_COUNT];
static Camera2D camera = {};
static bool follow_target;
static float delta;

// Random Float Function
static inline float random_float() { return rand() / (float)RAND_MAX; }

// Update Camera Function
static inline void update_camera() {
  camera.offset.x = .5f * GetScreenWidth();
  camera.offset.y = .5f * GetScreenHeight();
  camera.zoom = .5f * MIN(camera.offset.x, camera.offset.y);
}

// Set Range Function
static inline void set_range(float new_range) {
  range_squared = new_range * new_range;
  range = new_range;
}

// Update Boids Direction Function
static inline void *update_boids_dir(void *args) {
  const size_t offset = *(size_t *)args;
  for (size_t i = offset; i < BOID_COUNT; i += THREAD_COUNT) {
    const Vector2 target =
        follow_target
            ? Vector2Subtract(GetScreenToWorld2D(GetMousePosition(), camera),
                              boids[i].pos)
            : (Vector2){0, 0};
    Vector2 separation = {}, alignment = {}, cohesion = {};
    size_t close_boids = 0;
    for (size_t j = 0; j < BOID_COUNT; j++) {
      const Vector2 rel_pos = Vector2Subtract(boids[i].pos, boids[j].pos);
      const float dist_squared = Vector2LengthSqr(rel_pos);
      if (dist_squared < range_squared && i != j) {
        if (dist_squared != 0)
          separation = Vector2Add(
              separation, Vector2Scale(rel_pos, 1 / sqrtf(dist_squared)));
        else {
          separation.x += 2 * random_float() - 1;
          separation.y += 2 * random_float() - 1;
        }
        alignment = Vector2Add(alignment, boids[j].dir);
        cohesion = Vector2Subtract(cohesion, rel_pos);
        close_boids++;
      }
    }
    if (close_boids != 0) {
      const float close_boids_recip = 1.f / close_boids;
      alignment = Vector2Scale(alignment, close_boids_recip);
      cohesion = Vector2Scale(cohesion, close_boids_recip);
    }

    boids[i].new_dir = Vector2Normalize(Vector2Add(
        boids[i].dir,
        Vector2Scale(
            Vector2Add(
                Vector2Scale(separation, separation_weight),
                Vector2Add(Vector2Scale(alignment, alignment_weight),
                           Vector2Add(Vector2Scale(cohesion, cohesion_weight),
                                      Vector2Scale(target, target_weight)))),
            delta)));
  }
  return NULL;
}

// Main Function
int main(int arg_count, char **args) {
  // Setup
  bool show_boids = true, show_range = true, show_tail = true;
  uint8_t selector = 0;

  SetConfigFlags(FLAG_WINDOW_RESIZABLE | FLAG_MSAA_4X_HINT);
  InitWindow(800, 600, "Boids");
  MaximizeWindow();

  const Font font = GetFontDefault();
  update_camera();

  for (size_t i = 0; i < BOID_COUNT; i++) {
    const float theta = 2 * PI * random_float();
    boids[i].pos.x = 2 * random_float() - 1;
    boids[i].pos.y = 2 * random_float() - 1;
    boids[i].dir.x = cosf(theta);
    boids[i].dir.y = sinf(theta);
  }
  set_range(.2f);

  // Game Loop
  while (!WindowShouldClose()) {
    // Update
    delta = GetFrameTime();

    if (IsWindowResized())
      update_camera();

    follow_target = IsMouseButtonDown(MOUSE_BUTTON_LEFT);

    if (IsKeyPressed(KEY_B))
      show_boids = !show_boids;
    if (IsKeyPressed(KEY_R))
      show_range = !show_range;
    if (IsKeyPressed(KEY_T))
      show_tail = !show_tail;

    if (IsKeyPressed(KEY_RIGHT)) {
      switch (selector) {
      case 0:
        set_range(1.25f * range);
        break;
      case 1:
        separation_weight *= 1.25f;
        break;
      case 2:
        alignment_weight *= 1.25f;
        break;
      case 3:
        cohesion_weight *= 1.25f;
        break;
      case 4:
        target_weight *= 1.25f;
      }
    }
    if (IsKeyPressed(KEY_LEFT)) {
      switch (selector) {
      case 0:
        set_range(.8f * range);
        break;
      case 1:
        separation_weight *= .8f;
        break;
      case 2:
        alignment_weight *= .8f;
        break;
      case 3:
        cohesion_weight *= .8f;
        break;
      case 4:
        target_weight *= .8f;
      }
    }

    if (IsKeyPressed(KEY_DOWN))
      if (selector != 4)
        selector++;
    if (IsKeyPressed(KEY_UP))
      if (selector != 0)
        selector--;

    pthread_t threads[THREAD_COUNT];
    size_t args[THREAD_COUNT];
    for (size_t i = 0; i < THREAD_COUNT; i++) {
      args[i] = i;
      pthread_create(&threads[i], NULL, update_boids_dir, &args[i]);
    }
    for (size_t i = 0; i < THREAD_COUNT; i++)
      pthread_join(threads[i], NULL);

    for (size_t i = 0; i < BOID_COUNT; i++) {
      boids[i].dir = boids[i].new_dir;
      boids[i].pos =
          Vector2Add(boids[i].pos, Vector2Scale(boids[i].dir, delta));

      if (fabsf(boids[i].pos.x) > 2) {
        boids[i].dir.x = -boids[i].dir.x;
        boids[i].pos.x = fmaxf(-2, MIN(boids[i].pos.x, 2));
      }
      if (fabsf(boids[i].pos.y) > 2) {
        boids[i].dir.y = -boids[i].dir.y;
        boids[i].pos.y = fmaxf(-2, MIN(boids[i].pos.y, 2));
      }
    }

    // Render
    BeginDrawing();
    ClearBackground(BLACK);

    BeginMode2D(camera);
    if (show_range)
      for (size_t i = 0; i < BOID_COUNT; i++)
        DrawCircleSector(boids[i].pos, range, 0, 360, 12,
                         (Color){255, 255, 255, MAX(1, 4 / (5 * range))});
    if (show_tail)
      for (size_t i = 0; i < BOID_COUNT; i++)
        DrawLineV(boids[i].pos,
                  Vector2Add(Vector2Scale(boids[i].dir, -.075f), boids[i].pos),
                  WHITE);
    if (show_boids)
      for (size_t i = 0; i < BOID_COUNT; i++)
        DrawCircleSector(boids[i].pos, .015f, 0, 360, 7, WHITE);
    EndMode2D();

    char info[256];
    snprintf(info, 256,
             "Frame Rate: %d FPS\n"
             "%sRange: %.2f\n"
             "%sSeparation Weight: %.2f\n"
             "%sAlignment Weight: %.2f\n"
             "%sCohesion Weight: %.f\n"
             "%sTarget Weight: %.2f",
             GetFPS(), selector == 0 ? "- " : "", range,
             selector == 1 ? "- " : "", separation_weight,
             selector == 2 ? "- " : "", alignment_weight,
             selector == 3 ? "- " : "", cohesion_weight,
             selector == 4 ? "- " : "", target_weight);
    DrawTextEx(font, info, (Vector2){10, 10}, 20, 2, WHITE);
    EndDrawing();
  }

  // Close
  UnloadFont(font);
  CloseWindow();
  return 0;
}
